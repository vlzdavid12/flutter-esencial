import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import '../models/category_models.dart';
import '../models/news_models.dart';

final _URL_NEWS = 'https://newsapi.org/v2';
final _APIKEY = '68700d15e4f245dcbbabb4d87d005e8b';

class NewsService with ChangeNotifier{
  List<Article> headLines =  [];
  String _selectedCategory =  'business';

  List<Category> categories =  [
    Category(FontAwesomeIcons.building, 'business'),
    Category(FontAwesomeIcons.tv, 'entertainment'),
    Category(FontAwesomeIcons.addressCard, 'general'),
    Category(FontAwesomeIcons.headSideVirus, 'health'),
    Category(FontAwesomeIcons.vials, 'science'),
    Category(FontAwesomeIcons.volleyball, 'sports'),
    Category(FontAwesomeIcons.memory, 'technology'),
  ];

  Map<String, List<Article>> categoryArticles = {};

  NewsService(){
    this.getTopHeadLines();
    categories.forEach((item) {
      categoryArticles[item.name] = [];
    });
    this.getArticlesByCategory(this.selectedCategory);
  }

  String get selectedCategory => this._selectedCategory;
  set selectedCategory(String value){
    this._selectedCategory = value;
    this.getArticlesByCategory(value);
    notifyListeners();
  }

  List<Article> get getArticlesCategorySelect =>  this.categoryArticles['business']!;

  getTopHeadLines() async{
   final url = Uri.parse('$_URL_NEWS/top-headlines?apiKey=$_APIKEY&country=us');
   final resp  =  await http.get(url);

   final newsResponse = newsResponseFromJson( resp.body);
   this.headLines.addAll(newsResponse.articles);
   notifyListeners();
  }

  getArticlesByCategory(String category) async {
    print(category);
    if(this.categoryArticles[category] != null){
      notifyListeners();
      return this.categoryArticles[category];
    }
    final url = Uri.parse('$_URL_NEWS/top-headlines?apiKey=$_APIKEY&country=us&category=general');
    final resp  =  await http.get(url);
    print('Result: $resp');
    final newsResponse = newsResponseFromJson( resp.body);
    this.categoryArticles[category]?.addAll(newsResponse.articles);
    notifyListeners();
  }

}
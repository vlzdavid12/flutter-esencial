import 'package:flutter/material.dart';

final myTheme = ThemeData.dark().copyWith(
  colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Colors.red),
  secondaryHeaderColor: Colors.red,
  primaryColor: Colors.grey,

);
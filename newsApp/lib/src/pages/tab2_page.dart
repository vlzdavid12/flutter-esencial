import 'package:flutter/material.dart';
import 'package:newsapp/src/models/category_models.dart';
import 'package:newsapp/src/services/news_service.dart';
import 'package:newsapp/src/theme/theme.dart';
import 'package:newsapp/src/widgets/list_news.dart';
import 'package:provider/provider.dart';

class Tab2Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final newsService = Provider.of<NewsService>(context);
    print(newsService.getArticlesCategorySelect);
    return SafeArea(
        child: Scaffold(
            body: Column(
      children: [
        _ListCategory(),
          Expanded(
          child: ListNews(newsService.getArticlesCategorySelect),
        )

      ],
    )));
  }
}

class _ListCategory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final categories = Provider.of<NewsService>(context).categories;

    return Container(
        width: double.infinity,
        height: 80,
        child: ListView.builder(
          physics: const BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: categories.length,
          itemBuilder: (BuildContext context, int index) {
            final cName = categories[index].name;
            return Container(
              width: 80,
              child: Padding(
                padding: EdgeInsets.all(8),
                child: Column(
                  children: [
                    _categoryButtom(category: categories[index]),
                    SizedBox(height: 5),
                    Text(
                      '${cName[0].toUpperCase()}${cName.substring(1)}',
                      style: TextStyle(fontSize: 10),
                    )
                  ],
                ),
              ),
            );
          },
        ));
  }
}

class _categoryButtom extends StatelessWidget {
  final Category category;

  const _categoryButtom({super.key, required this.category});

  @override
  Widget build(BuildContext context) {
    final newsService = Provider.of<NewsService>(context);
    return GestureDetector(
      onTap: () {
        final newsService = Provider.of<NewsService>(context, listen: false);
        newsService.selectedCategory = category.name;
      },
      child: Container(
        width: 40,
        height: 40,
        decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.white),
        child: Icon(
          category.icon,
          color: (newsService.selectedCategory == this.category.name)
              ? myTheme.secondaryHeaderColor
              : Colors.black54,
        ),
      ),
    );
  }
}

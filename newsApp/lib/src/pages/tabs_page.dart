import 'package:flutter/material.dart';
import 'package:newsapp/src/pages/tab1_page.dart';
import 'package:newsapp/src/pages/tab2_page.dart';
import 'package:newsapp/src/services/news_service.dart';
import 'package:newsapp/src/theme/theme.dart';
import 'package:provider/provider.dart';

class TabsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(create: (_) => new  _NavigationModel(),
    child:  Scaffold(
      body: _Pages(),
      bottomNavigationBar: _Navigation(),
    ),);

  }
}

class _Navigation extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final navegationModel = Provider.of<_NavigationModel>(context);
    //final newService = Provider.of<NewsService>(context);
    return BottomNavigationBar(
      currentIndex: navegationModel.pageState,
      onTap: (i) => navegationModel.pageState = i,
      unselectedItemColor: myTheme.primaryColor,
      selectedItemColor: myTheme.secondaryHeaderColor,
      items: [
      BottomNavigationBarItem(icon: Icon(Icons.person_outline), label: 'Para Ti'),
      BottomNavigationBarItem(icon: Icon(Icons.public), label: 'Encabezado')
    ],);
  }
}

class _Pages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final navigation = Provider.of<_NavigationModel>(context);
    return PageView(
      controller: navigation.pageController,
      physics: NeverScrollableScrollPhysics(),
      children: [
        Tab1Page(),
        Tab2Page()
      ],
    );
  }
}

class _NavigationModel with  ChangeNotifier{
  int _pageState =  0;
  PageController _pageController = new PageController(initialPage: 0);
  int get pageState => this._pageState;
  set pageState(int value){
    this._pageState = value;
    _pageController.animateToPage(value, duration: Duration(milliseconds: 250), curve: Curves.easeOut);
    notifyListeners();
  }
  PageController get pageController => this._pageController;

}

import 'package:flutter/material.dart';
import 'package:newsapp/src/theme/theme.dart';

import '../models/news_models.dart';

class ListNews extends StatelessWidget {

  final List<Article> news;
  const ListNews(this.news);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount:  this.news.length,
      itemBuilder: (BuildContext context, int index){
        return _News(post: this.news[index], index: index);
      }
    );
  }
}

class _News extends StatelessWidget {

  final Article post;
  final int index;

  const _News({super.key, required this.post, required this.index});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _CardTopBar(post: this.post, index: this.index),
        _CardTitle(post: this.post),
        _CardImage(post: this.post),
        _CardBody(post: this.post),
        SizedBox(height: 10),
        Divider(),
        _CardButons(),
      ],
    );
  }
}


class _CardButons extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RawMaterialButton(onPressed: (){},
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            child: Icon(Icons.start_outlined),
          ),
          SizedBox(width: 30),
          RawMaterialButton(onPressed: (){},
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            child: Icon(Icons.more_outlined),
          )
        ],
      )
    );
  }
}


class _CardBody extends StatelessWidget {

  final Article post;

  const _CardBody({super.key, required this.post});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Text((post.description != null)? post.description.toString() : ''),
    );
  }
}


class _CardImage extends StatelessWidget {
  final Article post;

  const _CardImage({super.key, required this.post});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: ClipRRect(
        borderRadius: BorderRadius.only(topLeft: Radius.circular(50), bottomRight: Radius.circular(50)),
        child: Container(
          child:
          (post.urlToImage != null)
              ?FadeInImage(
            placeholder: const AssetImage('assets/img/giphy.gif'),
            image: NetworkImage(post.urlToImage!),
          ): Image(image: AssetImage('assets/img/no-image.png')),
        ),
      ),
    );
  }
}


class _CardTitle extends StatelessWidget {

  final Article post;

  const _CardTitle({super.key, required this.post});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Text(post.title, style: TextStyle(fontSize: 14, fontWeight: FontWeight.w700),),
    );
  }
}




class _CardTopBar extends StatelessWidget {
  final Article post;
  final int index;
  const _CardTopBar({super.key, required this.post, required this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10),
      margin: EdgeInsets.only(bottom: 10),
      child: Row(
        children: [
          Text('${index + 1}. ', style:  TextStyle(color: myTheme.colorScheme.secondary)),
          Text('${post.source.name}')
        ],

      ),
    );
  }
}


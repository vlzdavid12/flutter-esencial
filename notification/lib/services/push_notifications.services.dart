//44:60:AD:67:EC:55:7A:6E:2C:4F:E8:39:6A:CB:73:19:A7:05:80:00
import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class PushNotificationService{
  static FirebaseMessaging messaging =  FirebaseMessaging.instance;
  static String? token;
  
  static final StreamController<String> _messageStream = StreamController.broadcast();
  static Stream<String> get messageStream => _messageStream.stream;

  static Future _backGroundHandler(RemoteMessage message) async{
    //print('background Handler ${message.messageId}');
    print(message.data);
    _messageStream.add(message.data['product'] ?? 'No  data');
  }

  static Future _onMessageHandler(RemoteMessage message) async{
    //print('message Handler ${message.messageId}');
    print(message.data);
    _messageStream.add(message.data['product'] ?? 'No  data');
  }

  static Future _onMessageOpenApp(RemoteMessage message) async{
    //print('open Handler ${message.messageId}');
    print(message.data);
    _messageStream.add(message.data['product'] ?? 'No  data');

  }

  static Future initialApp() async{
    //Push Notification
    await Firebase.initializeApp();
    await requestPermission();
    token =  await FirebaseMessaging.instance.getToken();
    print(token);

    //Handlers
    FirebaseMessaging.onBackgroundMessage(_backGroundHandler);
    FirebaseMessaging.onMessage.listen(_onMessageHandler);
    FirebaseMessaging.onMessageOpenedApp.listen(_onMessageOpenApp);

    //Local Notification
  }


  //Apple or Web Permission
  static requestPermission() async {
    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      criticalAlert: false,
      carPlay: false,
      provisional: false,
      sound: true
    );
    print('User Push Notification Statud ${settings.authorizationStatus}');
  }

  static closeStreams(){
    _messageStream.close();
  }

}
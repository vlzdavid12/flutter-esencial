import 'package:flutter/material.dart';
import 'package:notification/screen/screen.dart';
import 'package:notification/services/push_notifications.services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await PushNotificationService.initialApp();
  runApp(MyApp());
}
class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  final GlobalKey<ScaffoldMessengerState> messengerKey = GlobalKey<ScaffoldMessengerState>();

  void initState(){
    super.initState();

    //CONTEXT!
    PushNotificationService.messageStream.listen((message) {
      //print('MyApp: $message');
      navigatorKey.currentState?.pushNamed('message', arguments:  message);
      final snackBar =  SnackBar(content: Text(message));
      messengerKey.currentState?.showSnackBar(snackBar);
    });

  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      initialRoute: 'home',
      navigatorKey: navigatorKey,
      scaffoldMessengerKey: messengerKey,
      routes: {
        'home': (_) => HomeScreen(),
        'message': (_) => MessageScreen(),
      },
    );
  }
}





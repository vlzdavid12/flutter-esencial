import 'package:flutter/material.dart';
import 'package:product_app/models/models.dart';
import 'package:product_app/screens/screens.dart';
import 'package:product_app/services/services.dart';
import 'package:product_app/widgets/widgets.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    final productService = Provider.of<ProductsService>(context);
    final authService = Provider.of<AuthService>(context, listen: false);
    if (productService.isLoading) return LoadingScreen();
    return Scaffold(
      appBar: AppBar(
        title: Text('Productos'),
        leading: IconButton(
          icon: Icon(Icons.login_outlined),
          onPressed: (){
              authService.logout();
              Navigator.pushReplacementNamed(context, 'login');
          },
        ),
      ),
      body: ListView.builder(
        itemCount: productService.products.length,
        itemBuilder: (BuildContext context, int index) => GestureDetector(
          child: ProductCard(
            product: productService.products[index],
          ),
          onTap: () => {
            productService.selectedProduct =  productService.products[index].copy(),
            Navigator.pushNamed(context, 'product'),
          },
        ),
      ),
        floatingActionButton:  FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            productService.selectedProduct =  new Product(
                available: false,
                name: '',
                price: 0);
            Navigator.pushNamed(context, 'product');
          }),
    );
  }
}

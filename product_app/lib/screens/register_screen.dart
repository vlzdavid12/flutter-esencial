import 'package:flutter/material.dart';
import 'package:product_app/providers/product_form_providers.dart';
import 'package:product_app/services/services.dart';

import 'package:product_app/ui/input_decoration.dart';
import 'package:product_app/widgets/widgets.dart';
import 'package:provider/provider.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AuthBackground(
        child: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 220),
                CardContainer(
                    child: Column(
                      children: [
                        SizedBox(height: 10),
                        Text('Create Account', style: Theme.of(context).textTheme.headline5),
                        SizedBox(height: 30),
                        ChangeNotifierProvider(
                            create: (_)=> LoginFormProvider(),
                            child: _LoginForm()),
                        SizedBox(height: 15),
                      ],
                    )),
                SizedBox(height: 50),
                TextButton(
                    onPressed: () => Navigator.pushReplacementNamed(context, 'login'),
                    style: ButtonStyle(
                        overlayColor: MaterialStateProperty.all(Colors.indigo.withOpacity(0.1)),
                        shape: MaterialStateProperty.all(StadiumBorder())
                    ),
                    child: Text('Now account',
                        style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: Colors.black87)) )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _LoginForm extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final loginForm = Provider.of<LoginFormProvider>(context);
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Form(
          key: loginForm.formKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Column(children: [
            TextFormField(
              autocorrect: false,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecorations.authInputDecoration(
                hintText: 'vlzdavid12@outlook.com',
                labelText: 'Correo Electrónico',
                prefixIcon: Icons.alternate_email_sharp,
              ),
              onChanged: (value) => loginForm.email = value,
              validator: (value){
                String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                RegExp regExp  = new RegExp(pattern);

                return regExp.hasMatch(value ?? '')
                    ? null
                    : 'The email is invalid.';

              },
            ),
            SizedBox(height: 20),
            TextFormField(
              autocorrect: false,
              obscureText: true,
              keyboardType: TextInputType.text,
              decoration: InputDecorations.authInputDecoration(
                hintText: '********',
                labelText: 'Contraseña',
                prefixIcon: Icons.lock_outline,
              ),
              onChanged: (value) => loginForm.password = value,
              validator: (value){
                return (value != null && value.length >= 6) ? null : 'The password is required, min 6';
              },
            ),
            SizedBox(height: 20),
            MaterialButton(
                shape: RoundedRectangleBorder(borderRadius:  BorderRadius.circular(10)),
                disabledColor:  Colors.grey,
                elevation:  0,
                color: Colors.deepPurple,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 80, vertical: 15),
                  child: Text(
                      loginForm.isLoading ? 'What Moment' : 'Go!',
                      style: TextStyle(color: Colors.white)),
                ),
                onPressed: loginForm.isLoading ?  null : () async {
                  FocusScope.of(context).unfocus();
                  final authService = Provider.of<AuthService>(context, listen: false);
                  if(!loginForm.isValidForm()) return;
                  loginForm.isLoading = true;
                  final String? errorMessage = await authService.createUser(loginForm.email, loginForm.password);

                  if(errorMessage == null){
                    Navigator.pushReplacementNamed(context, 'home');
                  } else{
                      print(errorMessage);
                      loginForm.isLoading = false;
                  }

                }),

          ]),
        ),
      ),
    );
  }
}

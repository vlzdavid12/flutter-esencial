import 'package:flutter/material.dart';
import 'package:preferences_app/providers/providers.dart';
import 'package:preferences_app/share_preferences/preferences.dart';
import 'package:preferences_app/widgets/widgets.dart';
import 'package:provider/provider.dart';

class SettingsScreens extends StatefulWidget {
  const SettingsScreens({Key? key}) : super(key: key);
  static String routerName = 'Settings';
  @override
  State<SettingsScreens> createState() => _SettingsScreensState();
}

class _SettingsScreensState extends State<SettingsScreens>{
/*  bool isDarkMode = false;
  int gender =  1;
  String name = 'Pedro';*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      drawer: const SideMenu(),
      body: SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Settings',
            style: TextStyle(fontSize: 45, fontWeight: FontWeight.w300),
          ),
          const Divider(),
          SwitchListTile.adaptive(
              value: Preferences.isDarkMode,
              title: const Text('DarkMode'),
              onChanged: (value) {
                Preferences.isDarkMode =  value;
                final themeProvider = Provider.of<ThemeProvider>(context, listen:false);
                value ? themeProvider.setDarkMode() : themeProvider.setLightMode();
                setState((){});
              }),
          const Divider(),
          RadioListTile<int>(
              value: 1,
              title: const Text('Masculino'),
              groupValue: Preferences.gender,
              onChanged: (value) {
                Preferences.gender = value ?? 1;
                setState((){});
              }),
          const Divider(),
          RadioListTile<int>(
              value: 2,
              title: const Text('Femenino'),
              groupValue: Preferences.gender,
              onChanged: (value) {
                Preferences.gender = value ?? 2;
                setState((){});
              }),
          const Divider(),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: TextFormField(
              initialValue: Preferences.name,
              onChanged: (value){
                Preferences.name = value;
                setState((){});
              },
              decoration: const InputDecoration(
                  labelText: 'Nombre', helperText: 'Nombre del usuario'),
            ),
          )
        ],
      )),
    );
  }
}

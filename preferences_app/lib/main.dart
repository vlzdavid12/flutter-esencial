import 'package:flutter/material.dart';
import 'package:preferences_app/providers/providers.dart';
import 'package:preferences_app/screens/screens.dart';
import 'package:preferences_app/share_preferences/preferences.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Preferences.init();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(
          create: (_) => ThemeProvider(isDarkMode: Preferences.isDarkMode))
    ],
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      initialRoute: HomeScreens.routerName,
      routes: {
        HomeScreens.routerName: (_) => HomeScreens(),
        SettingsScreens.routerName: (_) => SettingsScreens(),
      },
      theme: Provider.of<ThemeProvider>(context).currentTheme,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:preferences_app/screens/screens.dart';
class SideMenu extends StatelessWidget {
  const SideMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          const _DrawHeader(),
          ListTile(
            leading: const Icon(Icons.pages_outlined),
            title: const Text('Home'),
            onTap: (){
              Navigator.pushReplacementNamed(context, HomeScreens.routerName);
            },
          ),
          ListTile(
            leading: const Icon(Icons.pages_outlined),
            title: const Text('People'),
            onTap: (){

            },
          ),
          ListTile(
            leading: const Icon(Icons.settings_outlined),
            title: const Text('Settings'),
            onTap: (){
                //Navigator.pop(context);
              Navigator.pushReplacementNamed(context, SettingsScreens.routerName);

            },
          )
        ],
      )
    );
  }
}

class _DrawHeader extends StatelessWidget {
  const _DrawHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DrawerHeader(
      child: Container(),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/menu-img.jpg'),
          fit: BoxFit.cover
        )
      ),
    );
  }
}

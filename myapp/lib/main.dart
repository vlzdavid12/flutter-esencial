import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:myapp/screens/basic_design.dart';
import 'package:myapp/screens/home_screen.dart';
import 'package:myapp/screens/scroll_design.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      initialRoute: 'home_screen',
      routes: {
        'basic_design':(_) => BasicDesignScreen(),
        'scroll_screen':(_) => ScrollDesign(),
        'home_screen':(_) => HomeScreen()
      },
    );
  }
}



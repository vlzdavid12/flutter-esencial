import 'dart:ui';

import 'package:flutter/material.dart';

class CardTable extends StatelessWidget {
  const CardTable({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Table(
      children: [
        TableRow(children: [
          _SingleCard(
              icon: Icons.pie_chart_sharp,
              title: 'General',
              color: Colors.blue),
          _SingleCard(
              icon: Icons.emoji_transportation,
              title: 'Transport',
              color: Colors.pink)
        ]),
        TableRow(children: [
          _SingleCard(
              icon: Icons.cloud_circle, title: 'Cloud', color: Colors.indigo),
          _SingleCard(
              icon: Icons.shopify_sharp, title: 'Shopping', color: Colors.amber)
        ]),
        TableRow(children: [
          _SingleCard(
              icon: Icons.confirmation_num_sharp,
              title: 'Config',
              color: Colors.black45),
          _SingleCard(
              icon: Icons.media_bluetooth_off,
              title: 'Media',
              color: Colors.brown)
        ]),
        TableRow(children: [
          _SingleCard(
              icon: Icons.movie, title: 'Entreteiment', color: Colors.cyan),
          _SingleCard(
              icon: Icons.heart_broken,
              title: 'Favorite',
              color: Colors.redAccent)
        ]),
      ],
    );
  }
}

class _SingleCard extends StatelessWidget {
  final IconData icon;
  final Color color;
  final String title;

  const _SingleCard(
      {Key? key, required this.icon, required this.color, required this.title})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _CardBackGround(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CircleAvatar(
            backgroundColor: this.color,
            child: Icon(this.icon, size: 30, color: Colors.white)),
        SizedBox(height: 10),
        Text(this.title, style: TextStyle(color: this.color, fontSize: 18))
      ],
    ));
  }
}

class _CardBackGround extends StatelessWidget {
  final Widget child;

  const _CardBackGround({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(5),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
              child: Container(
                  height: 180,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(67, 66, 107, 0.6),
                      borderRadius: BorderRadius.circular(20)),
                  child: this.child)),
        ));
  }
}

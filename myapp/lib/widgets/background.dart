import 'dart:math';

import 'package:flutter/material.dart';

class Backgorund extends StatelessWidget {
  const Backgorund({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final  boxDecoration = BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          stops: [0.2, 0.8],
          colors: [Color(0xff3F4277), Color(0xff2B2B4C)]
        )
      );

    return Stack(
      children: [
        //Background Gradient
        Container(
          decoration: boxDecoration,
        ),
        // Ping Box
        Positioned(top: -100, left: -30, child: _PingBox())
      ],
    );
  }
}

class _PingBox extends StatelessWidget {
  const _PingBox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Transform.rotate( angle: -pi/5, child:  Container(
      width: 360,
      height: 360,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(80),
          gradient: LinearGradient(
              colors: [
                Color.fromRGBO(236, 98, 188, 1),
                Color.fromRGBO(241, 142, 172, 1),
              ]
          )
      ),
    ));
  }
}

import 'package:flutter/material.dart';

class BasicDesignScreen extends StatelessWidget {
  const BasicDesignScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      children: [Image(image: AssetImage('assets/landscape.jpeg')),
        _Title(),
        _ButtonSection(),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          child:   Text('orem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.')
        ),
            ],
    ));
  }
}

class _Title extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Description list campround', style:  TextStyle(fontWeight: FontWeight.bold),),
              Text('Description list campround', style: TextStyle(color: Colors.black45)),
            ],
          ),
          Expanded(child: Container()),
          Icon(Icons.star, color: Colors.red),
          Text('A41')
        ],
      ),

    );
  }
}

class _ButtonSection extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _CustomButton(icon: Icons.call, text: 'Call'),
          _CustomButton(icon:Icons.map_sharp, text: 'Route' ),
          _CustomButton(icon: Icons.share, text: 'Share'),
        ],
      ),
    );
  }
}

class _CustomButton extends StatelessWidget {
  final IconData icon;
  final String text;

  const _CustomButton({
    Key? key, required this.icon, required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
    children: [
      Icon(this.icon, color: Colors.blue, size: 30,),
      Text(this.text, style: TextStyle(color:  Colors.blue))
        ]
    );
  }
}

import 'package:flutter/material.dart';

final boxDecoration = BoxDecoration(gradient: LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    stops: [0.5,0.5],
    colors: [ Color(0xff7AECCA), Color(0xff50C2DD) ]
));

class ScrollDesign extends StatelessWidget {
  const ScrollDesign({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(
        decoration: boxDecoration,
     child: PageView(
          physics: BouncingScrollPhysics(),
          scrollDirection: Axis.vertical,
          children: [
            Page1(),
            Page2()
          ],
        ),
      ),
    );
  }
}



class _Background extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff50C2DD),
      height: double.infinity,
      alignment: Alignment.topCenter,
      child:  Image(image: AssetImage('assets/fondo.png')),
    );
  }
}

class Page1 extends StatelessWidget {
  const Page1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        //Background
          _Background(),
          //Main Content
          _MainContent()

      ],
    );
  }
}

class Page2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Color(0xff50C2DD),
      child: Center(
        child: TextButton(
          onPressed: (){},
          child:Padding(
            padding: EdgeInsets.symmetric(horizontal: 40),
            child:  Text('Wellcome', style: TextStyle(color: Colors.white, fontSize: 30),),
          ),
          style: TextButton.styleFrom(
              backgroundColor: Color(0xff1D52A0),
              shape: StadiumBorder()
          ),
        ),
      )
    );
  }
}


class _MainContent extends StatelessWidget {
  const _MainContent({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textStyle = TextStyle(fontSize: 50, fontWeight: FontWeight.bold, color: Colors.white);
    return SafeArea(
        bottom: false,
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: 30),
        Text("11°", style: textStyle),
        Text("Miercoles", style: textStyle),
        Expanded(child: Container()),
        Icon(Icons.keyboard_arrow_down, size: 100, color: Colors.white)

      ],
    ));
  }
}
import 'package:flutter/material.dart';
import 'package:myapp/widgets/card_table.dart';
import 'package:myapp/widgets/custom_bottom_navigation.dart';
import 'package:myapp/widgets/pageTitle.dart';

import '../widgets/background.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          //Background
          Backgorund(),
          _HomeBody(),
        ],
      ),
      bottomNavigationBar: CustomButtomNavigation(),
    );
  }
}

class _HomeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          //Titles
          PageTitle(),
          //Car Table
          CardTable()

        ],
      ),
    );
  }
}

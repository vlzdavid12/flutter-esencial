import 'package:flutter/material.dart';
import 'package:qrmap/widgets/scan_tiles.dart';

import '../providers/scan_list_provider.dart';
class AddressPage extends StatelessWidget {
  const AddressPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScanTiles(type: 'http');
  }
}
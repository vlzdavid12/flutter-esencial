import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import '../models/scan_model.dart';

class MapPage extends StatefulWidget {
  @override
  State<MapPage> createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {
  Completer<GoogleMapController> _controller = Completer();
  MapType mapType =  MapType.normal;
  @override
  Widget build(BuildContext context) {
    final ScanModel  scan =  ModalRoute.of(context)?.settings.arguments as ScanModel;
    final CameraPosition _kLake = CameraPosition(
        target: scan.getLatLng(),
        zoom: 17,
        tilt: 50);

    //Markers
    Set<Marker> markers = new Set<Marker>();
    markers.add(new Marker(
      markerId: MarkerId('geo-location'),
      position: scan.getLatLng()
    ));

    return Scaffold(
      appBar:  AppBar(
        elevation: 0,
        title: Text('Map Page'),
        actions: [
          IconButton(onPressed: () async {
            final GoogleMapController controller = await _controller.future;
            controller.animateCamera(
                CameraUpdate.newCameraPosition(
                  CameraPosition(target: scan.getLatLng(), zoom: 17, tilt: 50)
                )
            );
          }, icon: Icon(Icons.location_city_sharp))
        ],
      ),
      body: GoogleMap(
        myLocationButtonEnabled: false,
        mapType: mapType,
        markers: markers,
        initialCameraPosition: _kLake,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.layers),
        onPressed: () {
          if(mapType == MapType.normal){
            mapType = MapType.satellite;
          }else{
            mapType =  MapType.normal;
          }

          setState((){});
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qrmap/page/address_page.dart';
import 'package:qrmap/page/mapas_page.dart';
import 'package:qrmap/providers/db_provider.dart';
import 'package:qrmap/providers/scan_list_provider.dart';
import 'package:qrmap/providers/ui_provider.dart';
import 'package:qrmap/widgets/custom_floating_bottom.dart';
import 'package:qrmap/widgets/custom_navigate_bar.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        elevation: 0,
        title: Text('Historial'),
        actions: [
          IconButton(onPressed: (){
            Provider.of<ScanListProvider>(context, listen: false).eraseAllScans();
          }, icon: Icon(Icons.delete_forever))
        ],
      ),
      body: _HomePageBody(),
      bottomNavigationBar: CustomNavigationBar(),
      floatingActionButton: CustomFloatingBottom(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

class _HomePageBody extends StatelessWidget {
  const _HomePageBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Get selected menu opt
    final uiProvider =  Provider.of<UiProvider>(context);
    // Change for sho of page respective
    final currentIndex =  uiProvider.selectedMeuOpt;
    // Sqlite read
    // final tempScan =  new ScanModel(value: "http://www.google.com");
    // DBProvider.db.newScan(tempScan);
    // DBProvider.db.getScanById(1).then((valor) => print(valor?.value));
    // DBProvider.db.getAllScans().then((value) => print(value));
    // DBProvider.db.deleteAllScansQuery().then(print);

    //Use scan List Provider
    final scanListProvider =  Provider.of<ScanListProvider>(context, listen: false);

    switch(currentIndex){
      case 0:
        scanListProvider.loaderScansForType('geo');
        return MapasPage();
      case 1:
        scanListProvider.loaderScansForType('http');
        return AddressPage();
      default:
        return MapasPage();
    }
  }
}


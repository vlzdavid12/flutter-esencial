import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qrmap/page/home_page.dart';
import 'package:qrmap/page/map_page.dart';
import 'package:qrmap/providers/scan_list_provider.dart';
import 'package:qrmap/providers/ui_provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => new UiProvider()),
          ChangeNotifierProvider(create: (_)=> new ScanListProvider())
        ], 
        child: MaterialApp(title: 'Qr Map',
      debugShowCheckedModeBanner: false,
      initialRoute: 'home',
      routes: {
        'home': (_) => HomePage(),
        'map': (_) => MapPage(),
      },
      theme: ThemeData(
        primaryColor: Colors.purple,
        colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.purple,
        ).copyWith(
          secondary: Colors.purple,
        ),
        textTheme: const TextTheme(bodyText2: TextStyle(color: Colors.grey)),
      ),
    ));
  }
}



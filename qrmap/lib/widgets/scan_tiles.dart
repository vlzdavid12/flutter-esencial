import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qrmap/utils/utils.dart';

import '../providers/scan_list_provider.dart';

class ScanTiles extends StatelessWidget {
  final String type;

  const ScanTiles({super.key, required this.type});

  @override
  Widget build(BuildContext context) {
    final scansListProvider = Provider.of<ScanListProvider>(context);
    final scans = scansListProvider.scans;
    return ListView.builder(
        itemCount: scans.length,
        itemBuilder: (_, i) => Dismissible(
            key: UniqueKey(),
            background: Container(
              color: Colors.red,
            ),
            onDismissed: (DismissDirection direction) {
              Provider.of<ScanListProvider>(context, listen: false)
                  .eraseScanById(scans[i].id!);
            },
            child: ListTile(
              leading: Icon(
                  this.type == 'http'? Icons.local_activity : Icons.map_outlined,
                  color: Theme.of(context).primaryColor),
              title: Text(scans[i].value),
              subtitle: Text('ID:' + scans[i].id.toString()),
              trailing: Icon(Icons.keyboard_arrow_right, color: Colors.grey),
              onTap: () => LaunchUrl(context, scans[i]),
            )));
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/ui_provider.dart';
class CustomNavigationBar extends StatelessWidget {
  const CustomNavigationBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Get selected menu opt
    final uiProvider =  Provider.of<UiProvider>(context);
    // Get Current State Index
    final currentIndex = uiProvider.selectedMeuOpt;

    return BottomNavigationBar(
      elevation: 0,
      onTap: (int i) => uiProvider.selectedMeuOpt = i,
      currentIndex: currentIndex,
      items:<BottomNavigationBarItem>[
        BottomNavigationBarItem(icon: Icon(Icons.map), label: 'Mapa' ),
        BottomNavigationBarItem(icon: Icon(Icons.compass_calibration), label: 'Direcciones' ),
      ]
    );
  }
}

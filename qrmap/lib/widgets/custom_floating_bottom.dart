import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:provider/provider.dart';
import 'package:qrmap/providers/scan_list_provider.dart';
import 'package:qrmap/utils/utils.dart';

class CustomFloatingBottom extends StatelessWidget {
  const CustomFloatingBottom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        elevation: 0,
        child: Icon(Icons.filter_center_focus),
        onPressed: () async{
          String barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
              '#3DBBEF',
              'Cancel',
              false,
              ScanMode.QR);
          //final barcodeScanRes = 'geo:4.7935241,-75.7255078';

          if(barcodeScanRes == '-1') return;

          final scanListProvider =  Provider.of<ScanListProvider>(context, listen: false);
          final newScan =  await scanListProvider.newScan(barcodeScanRes);

          LaunchUrl(context, newScan);
        }
    );
  }
}

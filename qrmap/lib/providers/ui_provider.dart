import 'package:flutter/material.dart';

class UiProvider extends ChangeNotifier{
  int _selectedMeuOpt = 0;
  int get selectedMeuOpt {
    return this._selectedMeuOpt;
  }
  set selectedMeuOpt(int i){
    this._selectedMeuOpt = i;
    notifyListeners();
  }
}
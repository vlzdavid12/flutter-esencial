import 'dart:io';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'package:path_provider/path_provider.dart';
import 'package:qrmap/models/scan_model.dart';
export 'package:qrmap/models/scan_model.dart';



class DBProvider {
  static Database?_dataBase;
  static final DBProvider db =  DBProvider._();

  DBProvider._();

  Future<Database?> get database async {
    if (_dataBase == null) {
      _dataBase = await _initDB();
    }
    return _dataBase;
  }

  Future<Database> _initDB() async{
    //Where path storage of the data base
    Directory documentsDirectory = await getApplicationDocumentsDirectory();

    final path = join(documentsDirectory.path, 'ScansDB.db');
    print(path);
    //Create Database
    return await openDatabase(
      path,
      version: 1,
      onOpen: (db){},
      onCreate: (Database db, int version) async {
        await db.execute('''
        CREATE TABLE  Scans(
          id INTEGER PRIMARY KEY,
          type TEXT,
          value TEXT
        )
        ''');
      }
    );



  }

  newScanRaw(ScanModel newScan) async {

    final id = newScan.id;
    final type = newScan.type;
    final value = newScan.value;

    //Verify of database
    final db = await database;
    final res = await db?.rawInsert('''
    INSERT INTO Scans(id, type, value) 
      VALUES ($id , '$type', '$value')
    ''');
    return res;
  }

  newScan(ScanModel newScan) async {
     final db = await database;
     final resp = await db?.insert('Scans', newScan.toJson());
     return resp;
  }

  Future<ScanModel?> getScanById(int id) async{
    final db =  await database;
    final resp = await db!.query('Scans', where: 'id = ?', whereArgs: [id] );

    return resp.isNotEmpty ? ScanModel.fromJson(resp.first) : null;

  }


  Future<List<ScanModel>?>getAllScans() async {
    final db =  await database;
    final resp = await db!.query('Scans');

    return resp.isNotEmpty ?
        resp.map((s) => ScanModel.fromJson(s)).toList()
        : [];
  }


  Future<List<ScanModel>?>getAllScansByType(String type) async {
    final db =  await database;
    final resp = await db!.rawQuery('''
    SELECT *  FROM Scans WHERE type = '$type'
    ''');

    return resp.isNotEmpty ?
    resp.map((s) => ScanModel.fromJson(s)).toList()
        : [];
  }

  Future<int> updateScan(ScanModel newScan) async {
    final db  = await database;
    final resp = await db!.update('Scans',  newScan.toJson(), where: 'id = ?', whereArgs: [newScan.id]);
    return resp;
  }


  Future<int> deleteScan(int id) async {
    final db = await database;
    final resp = await db!.delete('Scans', where: 'id = ?', whereArgs: [id]);
    return resp;
  }


  Future<int> deleteAllScans() async {
    final db = await database;
    final resp = await db!.delete('Scans');
    return resp;
  }


  Future<List<Map<String, Object?>>> deleteAllScansQuery() async {
    final db = await  database;
    final resp =  await db!.rawQuery('''
    DELETE FROM Scans 
    ''');
    return resp;
  }

}
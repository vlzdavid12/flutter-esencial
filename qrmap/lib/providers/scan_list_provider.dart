import 'package:flutter/material.dart';
import 'package:qrmap/providers/db_provider.dart';
import '../models/scan_model.dart';

class ScanListProvider extends ChangeNotifier {
  List<ScanModel> scans =  [];
  String typeSelect = 'http';

  Future<ScanModel> newScan(String value) async {
    final newScan = new ScanModel(value: value);
    final id = await DBProvider.db.newScan(newScan);

    //Assign the Id Database of model
    newScan.id = id;

    if(this.typeSelect == newScan.type){
      this.scans.add(newScan);
      notifyListeners();
    }

    return newScan;
  }

  loaderScans() async {
    final result =  await DBProvider.db.getAllScans();
    this.scans =  [...result!];
    notifyListeners();
  }

  loaderScansForType(String type ) async {
    final result =  await DBProvider.db.getAllScansByType(type);
    this.scans =  [...result!];
    this.typeSelect = type;
    notifyListeners();
  }

  eraseAllScans() async {
    await DBProvider.db.deleteAllScans();
    this.scans = [];
    notifyListeners();
  }

  eraseScanById(int id) async {
    await DBProvider.db.deleteScan(id);
    this.loaderScansForType(this.typeSelect);
  }

}